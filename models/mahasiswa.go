package models

import "time"

type Mahasiswa struct {
	ID        int       `gorm:"primary_key" json:"id"`
	Nama      string    `json:"nama"`
	Jurusan   string    `json:"jurusan"`
	CreatedAt time.Time `json:"create_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
