package main

import (
	"jcc-rest-api/config"
	"jcc-rest-api/routes"
)

func main() {
	db := config.ConnectDatabase()
	sqlDB, _ := db.DB()

	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}
