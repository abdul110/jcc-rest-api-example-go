package routes

import (
	"jcc-rest-api/controllers"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// middleware
	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"admin":  "password",
		"editor": "rahasia",
	}))

	r.GET("/api/mahasiswa", controllers.GetAllMahasiswa)
	authorized.POST("/api/mahasiswa", controllers.CreateMahasiswa)
	r.GET("/api/mahasiswa/:id", controllers.GetMahasiswaId)
	authorized.PUT("/api/mahasiswa/:id", controllers.UpdateMahasiswa)
	authorized.DELETE("/api/mahasiswa/:id", controllers.DeleteMahasiswa)

	return r
}
