package controllers

import (
	"jcc-rest-api/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type MahasiswaInput struct {
	Nama    string `json:"nama"`
	Jurusan string `json:"jurusan"`
}

// get all mahasiswa
func GetAllMahasiswa(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var mhs []models.Mahasiswa

	db.Find(&mhs)

	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

// create mahasiswa
func CreateMahasiswa(c *gin.Context) {
	var input MahasiswaInput
	// json check
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// create mahasiswa
	mhs := models.Mahasiswa{Nama: input.Nama, Jurusan: input.Jurusan}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&mhs)

	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

// get mahasiswa by id
func GetMahasiswaId(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var mhs models.Mahasiswa

	if err := db.Where("id = ?", c.Param("id")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": "Record Not Found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

// udpate mahasiswa
func UpdateMahasiswa(c *gin.Context) {
	var input MahasiswaInput
	var mhs models.Mahasiswa
	var updatedInput models.Mahasiswa
	db := c.MustGet("db").(*gorm.DB)

	// json check
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", c.Param("id")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": "Record Not Found"})
		return
	}
	// update mahasiswa
	updatedInput.UpdatedAt = time.Now()
	updatedInput.Nama = input.Nama
	updatedInput.Jurusan = input.Jurusan

	db.Model(&mhs).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

// delete mahasiswa by id
func DeleteMahasiswa(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var mhs models.Mahasiswa

	if err := db.Where("id = ?", c.Param("id")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": "Record Not Found"})
		return
	}

	db.Delete(&mhs)
	c.JSON(http.StatusOK, gin.H{"data": "data was successfully deleted"})
}
